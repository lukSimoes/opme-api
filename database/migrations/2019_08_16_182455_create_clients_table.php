<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',100);
            $table->string('corporate_name',100);
            $table->string('cnpj',14);
            $table->string('email',200);
            $table->string('logo',400)->nullable();
            $table->string('neighborhood',400);
            $table->string('street',400);
            $table->integer('number');
            $table->string('add_on_address',400)->nullable();
            $table->unsignedBigInteger('id_city');
            $table->foreign('id_city')->references('id')->on('cities');
            $table->decimal('customer_value_appear', 8, 2)->nullable();
            $table->decimal('value_supplier_appear', 8, 2)->nullable();
            $table->decimal('deal_customer_value', 8, 2)->nullable();
            $table->decimal('proposed_value_trading', 8, 2)->nullable();
            $table->decimal('negotiated_value_trading', 8, 2)->nullable();
            $table->decimal('negotiatedC_value_trading', 8, 2)->nullable();
            $table->boolean('status')->default(true);
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
