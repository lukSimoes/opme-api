<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Responsible extends Model
{
    //

    //

    protected $table ="responsibles";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','telefone','celular','email','status','client_id',
    ];
}
