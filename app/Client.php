<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //

    //

    protected $table ="clients";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','cnpj','email','logo','street','complemento_endereco','number','neighborhood','id_city',
        'customer_value_appear','value_supplier_appear','deal_customer_value','proposed_value_trading',
        'negotiated_value_trading','negotiatedC_value_trading','corporate_name','status'
    ];

    public function responsibles(){
        return $this->hasMany('App\Responsible','client_id');
    }

    public function city(){
        return $this->belongsTo('App\City','id_city');
    }
}
