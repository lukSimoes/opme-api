<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;


class AuthController extends Controller
{
    //

//   /* //Function to login in the api.
//    public function login() {
//        // get email and password from request
//        $credentials = request(['email', 'password']);
//
//        // try to auth and get the token using api authentication
//        if (!$token = auth('api')->attempt($credentials)) {
//            // if the credentials are wrong we send an unauthorized error in json format
//            return response()->json(['error' => 'Unauthorized'], 401);
//        }
//        return response()->json([
//            'token' => $token,
//            'type' => 'bearer', // you can ommit this
//            'expires' => auth('api')->factory()->getTTL() * 60, // time to expiration
//
//        ]);
//    }*/

    // Autenticação padrão, porém com o JWT.
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        if (! $token = JWTAuth::attempt($credentials)) {
            return response()->json(['error' => 'Credenciais inválidas'], 401);
        }
        $user = User::where(['email'=>$credentials['email']])->first();
        $user->phones;
        return response()->json([
            'status' => 'success',
            'token' => $token,
            'user' => $user
        ]);
    }
    // Renovação de Token
    public function refresh()
    {
        $token = JWTAuth::getToken();
        $newToken = JWTAuth::refresh($token);
        return response()->json([
            'token' => $newToken
        ]);
    }
    // Retorna as informações da sessão atual
    public function me(){
        return response()->json(Auth::user());
    }
    // Invalida a sessão atual
    public function logout(){
        $token = JWTAuth::getToken();
        JWTAuth::invalidate($token);
        return response()->json([
            'status' => 'success'
        ]);
    }
}
