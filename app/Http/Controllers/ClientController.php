<?php

namespace App\Http\Controllers;

use App\Client;
use App\Responsible;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['']]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $data = $request->all();

        $models =  Client::with(['responsibles','city.state']);
        if(isset($data['nome']))
            $models->where(['name'=>$data['nome']]);
        if(isset($data['cnpj']))
            $models->where(['cnpj'=>$data['cnpj']]);
        if(isset($data['razaosocial']))
            $models->where(['corporate_name'=>$data['razaosocial']]);

        $models =  $models->get();
        return response()->json($models,201);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $model = new Client();

        $data = $request->all();

        $validator = \Validator::make($data, [
            'CNPJ' => 'required|max:16',
            'name' => 'required',
            'email' => 'required|email',
            'id_city' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message'   => 'Falha na validação',
                'errors'    => $validator->errors()->all()
            ], 422);
        }

        $model->fill($data);
        $model->user_id = Auth::user()->id;
        if( $model->save() ) {
           $this->addResponsible($data,$model->id);
            return response()->json($model, 201);
        };

        return response()->json([
            'message'   => 'Error ao salvar',
            'errors'    => $model
        ], 400);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Client::with(['responsibles','city.state'])->where(['id'=>$id])->first();
        if(!$model) {
            return response()->json([
                'message'   => 'Nada foi encontrado',
            ], 404);
        }
        return response()->json($model, 200);

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $model = Client::find($id);

        if(!$model) {
            return response()->json([
                'message'   => 'Nada foi encontrado',
            ], 404);
        }

        $data = $request->all();



        $validator = \Validator::make($data, [
            'CNPJ' => 'required|max:16',
            'name' => 'required',
            'email' => 'required|email',
            'id_city' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json([
                'message'   => 'Falha na validação',
                'errors'    => $validator->errors()->all()
            ], 422);
        }

        $model->fill($data);

        if( $model->save() ) {
            $this->addResponsible($data,$model->id);
            return response()->json($model);
        };

        return response()->json([
            'message'   => 'Error ao salvar',
            'errors'    => $model
        ], 400);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $model = Client::find($id);

        if(!$model) {
            return response()->json([
                'message'   => 'Nada foi encontrado',
            ], 404);
        }

        if($model->delete()){
            return response()->json([
                'message'   => 'Deletado com sucesso.',
            ], 200);
        }

        return response()->json([
            'message'   => 'Ops! não pode ser deletado :/',
        ], 200);
    }
    /**
     * Adding the responsibles for one client
     *
     * @param  int  $id
     * @param  array  $data
     * @return VOID
     */
    public function addResponsible($data, $id) {
        if(isset($data['responsibles'])){
            Responsible::where(['client_id'=>$id])->delete();
            foreach ($data['responsibles'] as $responsible) {
                $modelResponsible = new Responsible();
                $modelResponsible->client_id = $id;
                $modelResponsible->name = $responsible['name'];
                $modelResponsible->telefone = $responsible['telefone'];
                $modelResponsible->celular = $responsible['celular'];
                $modelResponsible->email = $responsible['email'];
                $modelResponsible->save();
            }
        }

    }
    /**
     * alter the status of a client
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function alterStatus(Request $request, $id) {
        $model = Client::find($id);

        if(!$model) {
            return response()->json([
                'message'   => 'Nada foi encontrado',
            ], 404);
        }

        $model->status = !$model->status;

        if( $model->save() ) {

            return response()->json($model);
        };
    }

    /**
     * alter the status of a client
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function alterStatusResponsible($client_id, $id) {
        $model = Responsible::where(['client_id'=>$client_id,'id'=>$id])->first();

        if(!$model) {
            return response()->json([
                'message'   => 'Nada foi encontrado',
            ], 404);
        }

        $model->status = !$model->status;

        if( $model->save() ) {

            return response()->json($model);
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeResponsible($client_id ,$id)
    {

        $model = Responsible::where(['client_id'=>$client_id,'id'=>$id])->first();

        if(!$model) {
            return response()->json([
                'message'   => 'Nada foi encontrado',
            ], 404);
        }

        if($model->delete()){
            return response()->json([
                'message'   => 'Deletado com sucesso.',
            ], 200);
        }

        return response()->json([
            'message'   => 'Ops! não pode ser deletado :/',
        ], 200);
    }
}
