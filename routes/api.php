<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('logout', 'AuthController@logout');
});
Route::get('client/all', 'ClientController@index');
Route::get('client/view/{id}', 'ClientController@show');
Route::post('client/save', 'ClientController@store');
Route::put('client/update/{id}', 'ClientController@update');
Route::get('client/alterStatus/{id}', 'ClientController@alterStatus');
Route::get('client/alterStatusResponsible/{client_id}/{id}', 'ClientController@alterStatusResponsible');


Route::delete('client/delete/{id}', 'ClientController@destroy');
Route::delete('client/removeResponsible/{client_id}/{id}', 'ClientController@removeResponsible');