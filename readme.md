
## API OPME

API desenvolvida como parte de um teste para OPME, api controla clientes e responsáveis, 

## END POINTS

Após toda a configuração você pode chamar a api criando um Vhost ou acessando diretamente pelo local host, localhost/nome-do-seu-projeto/public

	- https://documenter.getpostman.com/view/1475842/SVfGzCGj

## Dependências

A API foi desenvolvid com LARAVEL 5.8, então  será necessário instalar algum servidor PHP, aconselho o pacote do WAMP que ja te fornece tudo que é necessário para rodar a api.

	- PHP 7.* >
	- MySQL

## Acesso
	- Login: admin@admin.com
	- Password: admin

## Como utilizar

1° Faça o clone do projeto utilizando o seguinte comando.

	- git clone git@bitbucket.org:lukSimoes/opme-api.git

2° Rode o composer para instalar as dependências

	- Compopser update

3° Crie um Banco de dados mysq, descomente o arquivo .env.example deixando apenas .env e coloque os dados de conexão.

	- DB_CONNECTION=mysql
	  DB_HOST=localhost
	  DB_PORT=3306
	  DB_DATABASE=api_opme
	  DB_USERNAME=root
	  DB_PASSWORD=

4° Gere a API KEY da sua aplicação

	- php artisan key:generate 

5° Como ultimo passo rode as migrations e os seeds

	-  php artisan migrate
	-  php artisan db:seed -v

6° Pronto a aplicação está configurada, você pode acessar api utilizando postman. 